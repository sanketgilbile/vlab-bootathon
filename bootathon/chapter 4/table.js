var cnt = document.getElementById("n1");
var table = document.getElementById("t1");
function mult() {
    var n = +(cnt.value);
    if (isNaN(n)) {
        alert("please enter value");
    }
    else {
        while (table.rows.length > 1) {
            table.deleteRow(1);
        }
        for (let i = 1; i <= n; i++) {
            var row = table.insertRow();
            var td1 = row.insertCell();
            var td2 = row.insertCell();
            var td3 = row.insertCell();
            td1.innerHTML = n.toString() + "*" + i.toString();
            td2.innerHTML = "=";
            td3.innerHTML = (n * i).toString();
        }
    }
}
//# sourceMappingURL=table.js.map