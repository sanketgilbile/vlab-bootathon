<h1>seperating and throttling calorimeter</h1>

<h3 ><b> Introduction </b></h3>
<i>The temperature change of a gas or liquid when it is forced through a valve or porous plug while kept insulated so that no heat is exchanged with the environment. This procedure is called a Throttling process. This temperature can be described by the “Joule–Thomson effect” or “Joule–Kelvin effect” or “Kelvin–Joule effect” or “Joule–Thomson expansion”.In the Joule experiment, the gas expands in a vacuum and the temperature drop of the system is zero, if the gas were ideal. In this process here is no change in enthalpy from state one to state two, h1 = h2; no work is done, W = 0; and the process is adiabatic, Q = 0. Let’s take an example of a throttling process is an ideal gas flowing through a valve in mid position. We can observe that: Pin > Pout, velin < velout (where P = pressure and vel = velocity). These observations confirm the theory that hin = hout. Remember h = u + PV (v = specific volume), so if pressure decreases then specific volume must increase if enthalpy is to remain constant (assuming u is constant). Because mass flow is constant, the change in specific volume is observed as an increase in gas velocity. The theory also states W = 0. Our observations again confirm this to be true as clearly no "work" has been done by the throttling process. Finally, the theory states that an ideal throttling process is adiabatic. This cannot clearly be proven by observation since a "real" throttling process is not ideal and will have some heat transfer. In this process, steam becomes drier and nearly saturated steam becomes, superheated. As a gas expands, the average distance between molecules grows. Because of intermolecular attractive forces , expansion causes an increase in the potential energy of the gas. If no external work is extracted in the process and no heat is transferred, the total energy of the gas remains the same because of the conservation of energy. The increase in potential energy thus implies a decrease in kinetic energy and therefore in temperature. A second mechanism has the opposite effect. During gas molecule collisions, kinetic energy is temporarily converted into potential energy. As the average intermolecular distance increases, there is a drop in the number of collisions per time unit, which causes a decrease in average potential energy. Again, total energy is conserved, so this leads to an increase in kinetic energy (temperature). Below the Joule–Thomson inversion temperature, the former effect (work done internally against intermolecular attractive forces) dominates, and free expansion causes a decrease in temperature. Above the inversion temperature, gas molecules move faster and so collide more often, and the latter effect (reduced collisions causing a decrease in the average potential energy) dominates: Joule–Thomson expansion causes a temperature increase.</i>

<h2> <b>Procedure </b></h2>
<i>
1. Cleaning the setup: Firstly clean the setup and fill your setup with distilled water.
2. Temperature set: Set the temperature of steam generator up to 400 K.
3. Maintain the pressure: Now slowly open the needle valve, maintain the constant gauge pressure.
4. Steady state is reached: When steady state is reached note the pressure difference from manometer and Temperature after throttling by thermometer.
5. Collecting the moisture: Collect the suspended moisture from the separating and throttling calorimeter then Weigh it.
6. Now at last calculate the dryness factor by the given formulae.</i>

<h2><b>Expertiment calculation</b></h2>
<table>
<tr>
 <th>t </th>
 <th>p </th>
 <th>t1</th>
 <th>p1</th>
 <th>h</th>
 </tr>
 <tr>
 <td>120.5</td>
 <td>45.4</td>
 <td>67.5</td>
 <td>88.9</td>
 <td>777</td>
 <tr>
 <td>78.5</td>
 <td>67.8</td>
 <td>77.8</td>
 <td> 88.9</td>
 <td>77.9 </td>
 </tr>